# Copyright (c) 2021 PaddlePaddle Authors. All Rights Reserve.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os

import cv2
import numpy as np
import paddle
from paddleseg.cvlibs import manager, Config
from paddleseg.utils import get_sys_env, logger
from paddleseg.transforms import Compose

from core import predict
import model
from dataset import MattingDataset
from transforms import Compose
from utils import get_image_list, estimate_foreground_ml


def parse_args():
    parser = argparse.ArgumentParser(
        description='PP-HumanSeg inference for video')
    parser.add_argument(
        '--clip_path',
        dest='clip_path',
        help='clip path',
        type=str,
        default=None)
    parser.add_argument(
        '--alpha_path',
        dest='alpha_path',
        help='alpha path',
        type=str,
        default=None)
    parser.add_argument(
        '--background_path',
        dest='background_path',
        help='background image path',
        type=str,
        default=None)
    parser.add_argument(
        '--save_dir',
        dest='save_dir',
        help='The directory for saving the inference results',
        type=str,
        default='./output')

    return parser.parse_args()



def main(args):
    env_info = get_sys_env()
    place = 'gpu' if env_info['Paddle compiled with cuda'] and env_info[
        'GPUs used'] else 'cpu'
    paddle.set_device(place)

    logger.info(args.clip_path + "\n "+ args.alpha_path + "\n "+  args.background_path)

    clip_list, clip_dir = get_image_list(args.clip_path)

    alpha_list, alpha_dir = get_image_list(args.alpha_path)
    
    background_image_list, background_image_dir = get_image_list(args.background_path)

    clipLength = len(clip_list)
    alphaLength = len(alpha_list)
    backgroundLength = len(background_image_list)

    print(clipLength)
    print(alphaLength)
    print(backgroundLength)

    for x, clip in enumerate(clip_list):
        print (f"compose ... {x+1}, {clipLength}")

        alpha = cv2.imread(os.path.join(alpha_dir, os.path.basename(alpha_list[x])))

        # read bg
        fg = cv2.imread(os.path.join(clip_dir, os.path.basename(clip)))

        bg = get_bg(background_image_list[clipLength % backgroundLength], fg.shape)

        alpha = alpha / 255.0
        
        fg = fg.astype('uint8')
        alpha = alpha.astype('uint8')

        com = alpha * fg + (1 - alpha) * bg
        com = com.astype('uint8')
        com_save_path = os.path.join(args.save_dir, os.path.basename(clip))
        cv2.imwrite(com_save_path, com)


def get_bg(background, img_shape):
    bg = np.zeros(img_shape)
    if background == 'r':
        bg[:, :, 2] = 255
    elif background is None or background == 'g':
        bg[:, :, 1] = 255
    elif background == 'b':
        bg[:, :, 0] = 255
    elif background == 'w':
        bg[:, :, :] = 255

    elif not os.path.exists(background):
        raise Exception('The --background is not existed: {}'.format(
            background))
    else:
        bg = cv2.imread(background)
        bg = cv2.resize(bg, (img_shape[1], img_shape[0]))
    return bg


if __name__ == "__main__":
    args = parse_args()
    main(args)
